# -*- coding: utf-8 -*-

# Standard library
import datetime
import os


from google.appengine.ext.webapp import template

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True
)

# Import project file
import models


class MainPage(webapp2.RequestHandler):

    def get(self):
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(title='Welcome!'))


class Users(webapp2.RequestHandler):

    def get(self):
        users_query = models.User.query()
        users = users_query.fetch()

        template = JINJA_ENVIRONMENT.get_template('users.html')
        output = template.render(title='Current Users', users=users)

        # Finally, we can get this sent to the client
        self.response.write(output)


class Signup(webapp2.RequestHandler):

    def get(self):
        template = JINJA_ENVIRONMENT.get_template('signup.html')
        self.response.write(template.render(title='Signup Form', form=self.request))

    def post(self):
        try:
            forename = self.request.get('forename')
            surname = self.request.get('surname')
            email = self.request.get('email')

            # This is constructed from 3 separate fields
            date_of_birth = datetime.date(
                int(self.request.get('dob_year')),
                int(self.request.get('dob_month')),
                int(self.request.get('dob_day'))
            )

            password = self.request.get('password')
            password_confirmation = self.request.get('password_confirmation')

            user = models.User(forename=forename,
                               surname=surname,
                               email=email,
                               date_of_birth=date_of_birth,
                               password=password
            )

            # Persist this user to datastore
            user.put()
        except Exception:
            template = JINJA_ENVIRONMENT.get_template('signup.html')
            self.response.write(template.render(title='Signing up.. almost there!', form=self.request))
        else:
            template = JINJA_ENVIRONMENT.get_template('thanks.html')
            self.response.write(template.render(title='Thanks for signing up'))


# This is exposed by app.yaml as the main entry point
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/users/list', Users),
    ('/users/new', Signup),
], debug=True)
