"""
I found the following resource on unittests for the GAE platform:
https://cloud.google.com/appengine/docs/python/tools/localunittesting
"""

import datetime
import os
import sys
import unittest

"""
There must be a better way of calling into the App Engine code but the package manager on Ubuntu didn't allow
the installation of additional components with `gcloud components install ...`
"""
sys.path.insert(1, os.environ.get('GOOGLE_APP_ENGINE', 'google-cloud-sdk/platform/google_appengine'))


from google.appengine.api import datastore_errors
from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.ext import testbed

# Local module imports
import models


class UserTest(unittest.TestCase):

    def setUp(self):
        """
        As per the referenced document, this resets the Datastores between tests.
        """

        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()

    def tearDown(self):
        self.testbed.deactivate()

    def add_user(self, **kwargs):
        """
        Small helper to add new uers
        """
        user = models.User(**kwargs)
        user.put()
        return user

    def test_user_insert(self):
        # There should be nothing here to start with
        self.assertEqual(len(models.User.query().fetch(10)), 0)

        for i in xrange(65, 70):
            letter = chr(i)

            # Populate with some slightly varied dummy data
            self.add_user(forename='Mr', surname=letter, date_of_birth=datetime.date(1900 + i, 1, 1),
                          email='user_{0}@example.com'.format(letter), password=letter*10)

        self.assertEqual(len(models.User.query().fetch(10)), 5)

    def test_oldest_users(self):
        self.add_user(forename='Mr', surname='A', date_of_birth=datetime.date(1980, 6, 30), email='me@a.net', password='passA')
        self.add_user(forename='Ms', surname='B', date_of_birth=datetime.date(1973, 2, 25), email='me@b.net', password='passB')
        self.add_user(forename='Mr', surname='C', date_of_birth=datetime.date(2005, 8, 15), email='me@c.net', password='passC')

        oldest_users = models.User.query().order(models.User.date_of_birth).fetch()
        oldest = oldest_users[0]
        self.assertEqual(oldest.date_of_birth.year, 1973)

        self.assertEqual([1973, 1980, 2005], [user.date_of_birth.year for user in oldest_users], 'Expected date of birth to be in ascending order')

    def test_password_hashing(self):
        user = self.add_user(forename='Unix', surname='Epoch', date_of_birth=datetime.date(1970, 1, 1),
                           email='unix.epoch@example.com', password='cleartext')

        self.assertNotEqual('cleartext', user.password, 'The password should be hashed')

    def test_user_date_of_birth(self):
        self.assertRaises(datastore_errors.BadPropertyError, models.User, forename='Future', surname='Baby',
                           date_of_birth=datetime.date(3000, 8, 15),
                           email='test@example.com',
                           password='test'
        )

if __name__ == '__main__':
    unittest.main()
