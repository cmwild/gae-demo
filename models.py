import datetime
import hashlib
import re

from google.appengine.api import datastore_errors
from google.appengine.ext import ndb


def email_validator(prop, value):
    """
    Perform a basic email check. Local part, a single "@", and at least one each of the domain part and suffix.
    """
    if not re.match('[^@]+@[^@]+\.[^@]+', value):
        raise datastore_errors.BadPropertyError(prop._name)

    # Looks good enough at this point
    return value

def date_of_birth_validator(prop, value):
    # Do not allow future DOBs

    if value > datetime.date.today():
        raise datastore_errors.BadPropertyError(prop._name)

    return value

class PasswordProperty(ndb.StringProperty):
    """
    Custom property, as per:
    https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/appengine/standard/ndb/property_subclasses/my_models.py
    """
    def _to_base_type(self, value):
        return hashlib.md5(value).hexdigest()


class User(ndb.Model):
    """
    Model for representing an individual User entry.

    A userid does not need to be explicitly declared as `ndb.Model` does that for us.
    """
    forename = ndb.StringProperty(indexed=False)
    surname = ndb.StringProperty(indexed=False)
    date_of_birth = ndb.DateProperty(validator=date_of_birth_validator)
    email = ndb.StringProperty(validator=email_validator)
    password = PasswordProperty(required=True)
